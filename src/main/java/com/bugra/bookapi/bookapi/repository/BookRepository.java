package com.bugra.bookapi.bookapi.repository;

import com.bugra.bookapi.bookapi.domain.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends CrudRepository<Book, Long> {
}
