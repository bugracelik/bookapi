package com.bugra.bookapi.bookapi.config;

import com.bugra.bookapi.bookapi.domain.Book;
import com.bugra.bookapi.bookapi.repository.BookRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.stream.Stream;

@Configuration
public class Configurations {
    @Bean
    ApplicationRunner init(BookRepository repository) {
        // Save our starter set of books
        return args -> {
            Stream.of(new Book(null, "Horton Hears a Who", "Dr. Seuss"), new Book(null, "A Brief History of Time", "Stephen Hawking"),
                    new Book(null, "Brave New World", "Aldous Huxley")).forEach(book -> {
                repository.save(book);
            });
            //retrieve them all, and print so that we see everything is wired up correctly
            repository.findAll().forEach(System.out::println);
        };
    }

}
